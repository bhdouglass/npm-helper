# NPM Helper

A tool to run yarn or npm based on your context. Pro tip: set this to `y` and `n` shortcuts for quicker invocation.
It will automatically run volta with the correct version based on the .nvmrc file (use this when you want to use volta, but your team uses nvm).
When the project is using npm (based on the lack of a yarn.lock file), it will automatically detect if you are running a script and then you won't need to do `n run script`, just run `n script`

## Building

NPM Helper is written in Rust. Use `cargo build` to create a debug binary, and `cargo build --release` for a production binary.

## License

Copyright (C) 2024 [Brian Douglass](http://bhdouglass.com/)

This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License version 3, as published
by the Free Software Foundation.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranties of MERCHANTABILITY, SATISFACTORY QUALITY, or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program.  If not, see <http://www.gnu.org/licenses/>.

