use std::env;
use std::fs;
use std::path::PathBuf;
use std::process::Command;

// Searches for the package.json file in the current directory and up to two levels up
fn find_package_json() -> Result<PathBuf, Box<dyn std::error::Error>> {
    let cwd = env::current_dir()?;
    let lookup = ["./", "../", "../.."];
    let mut package_path: Option<PathBuf> = None;
    for prefix in lookup {
        let path = cwd.join(prefix).join("package.json");
        if path.exists() {
            package_path = Some(path);
            break;
        }
    }

    if package_path == None {
        return Err("No package.json found".into());
    }

    return Ok(package_path.unwrap());
}

// Searches for the yarn.lock file in the current directory and up to two levels up
fn is_yarn() -> Result<bool, Box<dyn std::error::Error>> {
    let cwd = env::current_dir()?;
    let lookup = ["./", "../", "../.."];
    let mut is_yarn = false;
    for prefix in lookup {
        let yarn_lock_path = cwd.join(prefix).join("yarn.lock");
        if yarn_lock_path.exists() {
            is_yarn = true;

            println!("DEBUG: found yarn.lock: {}", yarn_lock_path.display());
            break;
        }

        let npm_lock_path = cwd.join(prefix).join("package-lock.json");
        if npm_lock_path.exists() {
            println!("DEBUG: found package-lock.json: {}", npm_lock_path.display());
            break;
        }
    }

    return Ok(is_yarn);
}

// Read the node version from .nvmrc if it exists
fn find_node_version(package_path: PathBuf) -> Result<Option<String>, Box<dyn std::error::Error>> {
    let nvmrc_path = package_path.parent().unwrap().join(".nvmrc");
    if nvmrc_path.exists() {
        let nvmrc_data = fs::read_to_string(nvmrc_path).expect("Unable to read the nvmrc file");
        return Ok(Some(nvmrc_data.trim().to_string()));
    }

    return Ok(None);
}

fn main() -> Result<(), Box<dyn std::error::Error>> {
    let mut args: Vec<String> = env::args().collect();
    args.remove(0);

    let package_path = find_package_json()?;
    println!("DEBUG: found package.json: {}", package_path.display());

    let use_yarn = is_yarn()?;
    let package_manager = if use_yarn { "yarn" } else { "npm" };
    if use_yarn {
        println!("DEBUG: detected yarn as the tool to run");
    }
    else {
        println!("DEBUG: detected npm as the tool to run");
    }

    if !use_yarn && args.len() == 1 {
        let package_data = fs::read_to_string(package_path.clone()).expect("Unable to read the package.json file");
        let json: serde_json::Value = serde_json::from_str(&package_data).expect("JSON was not well-formatted");
        let scripts = json.get("scripts");
        if scripts != None {
            let name = args[0].clone();
            let script = scripts.unwrap().get(&name);
            if script != None {
                args.remove(0);
                args.push("run".to_string());
                args.push(name.clone());

                println!("DEBUG: detected npm script, injecting run");
            }
        }
    }

    let node_version = find_node_version(package_path)?;
    let mut command = Command::new(package_manager);
    if node_version != None {
        let version = node_version.unwrap();
        println!("DEBUG: detected node version {:?} from .nvmrc", version);

        command = Command::new("volta");
        command.arg("run").arg("--node").arg(version).arg(package_manager);
    }
    for arg in args {
        command.arg(arg);
    }

    println!("DEBUG: going to run command: {:?}", command);

    command.status()?;

    Ok(())
}
